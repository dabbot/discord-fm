mod error;

pub use error::{Error, Result};

use log::{trace, warn};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

const PLAYLISTS: &'static [(&'static str, &'static [u8], &'static [u8])] = &[
    (
        "Chill_Corner.json",
        include_bytes!("../json/Chill_Corner.json"),
        include_bytes!("../blob/Chill_Corner.json"),
    ),
    (
        "Christmas.json",
        include_bytes!("../json/Christmas.json"),
        include_bytes!("../blob/Christmas.json"),
    ),
    (
        "Classical.json",
        include_bytes!("../json/Classical.json"),
        include_bytes!("../blob/Classical.json"),
    ),
    (
        "Coffee-house_Jazz.json",
        include_bytes!("../json/Coffee-house_Jazz.json"),
        include_bytes!("../blob/Coffee-house_Jazz.json"),
    ),
    (
        "Electro_Hub.json",
        include_bytes!("../json/Electro_Hub.json"),
        include_bytes!("../blob/Electro_Hub.json"),
    ),
    (
        "Electro_Swing.json",
        include_bytes!("../json/Electro_Swing.json"),
        include_bytes!("../blob/Electro_Swing.json"),
    ),
    (
        "Halloween.json",
        include_bytes!("../json/Halloween.json"),
        include_bytes!("../blob/Halloween.json"),
    ),
    (
        "Hip-hop.json",
        include_bytes!("../json/Hip-hop.json"),
        include_bytes!("../blob/Hip-hop.json"),
    ),
    (
        "Japanese_Lounge.json",
        include_bytes!("../json/Japanese_Lounge.json"),
        include_bytes!("../blob/Japanese_Lounge.json"),
    ),
    (
        "Korean_Madness.json",
        include_bytes!("../json/Korean_Madness.json"),
        include_bytes!("../blob/Korean_Madness.json"),
    ),
    (
        "Metal_Mix.json",
        include_bytes!("../json/Metal_Mix.json"),
        include_bytes!("../blob/Metal_Mix.json"),
    ),
    (
        "Purely_Pop.json",
        include_bytes!("../json/Purely_Pop.json"),
        include_bytes!("../blob/Purely_Pop.json"),
    ),
    (
        "Retro_Renegade.json",
        include_bytes!("../json/Retro_Renegade.json"),
        include_bytes!("../blob/Retro_Renegade.json"),
    ),
    (
        "Rock-n-Roll.json",
        include_bytes!("../json/Rock-n-Roll.json"),
        include_bytes!("../blob/Rock-n-Roll.json"),
    ),
];

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct OidId {
    #[serde(rename = "$oid")]
    pub id: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum Id {
    Oid(OidId),
    Raw(String),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SongInfo {
    #[serde(rename = "_id")]
    pub id: Id,
    pub service: String,
    pub identifier: String,
    pub length: f64,
    pub title: String,
}

#[derive(Clone, Debug)]
pub struct PlaylistItem {
    pub info: SongInfo,
    pub track: String,
}

#[derive(Clone, Debug)]
pub struct Library {
    pub name: String,
    pub items: Vec<PlaylistItem>,
}

#[derive(Clone, Debug)]
pub struct DiscordFm {
    pub list: String,
    pub libraries: HashMap<String, Library>,
}

impl DiscordFm {
    pub fn new() -> Result<Self> {
        let mut libraries = HashMap::new();

        trace!("Walking over D.FM lists");

        for (name, json, blob) in PLAYLISTS {
            trace!("Deserializing playlist JSON");
            let songs: Vec<SongInfo> = serde_json::from_slice(json)?;
            trace!("Deserialized playlist JSON");

            let mut legible = (*name).to_owned();

            if let Some(pos) = name.rfind('.') {
                legible.truncate(pos);
            }

            legible = legible.replace('_', " ");

            trace!("Deserializing playlist blobs");
            let blobs: Vec<String> = serde_json::from_slice(blob)?;
            trace!("Deserialized playlist blobs");

            let blob_len = blobs.len();
            trace!("JSONs: {}; blobs: {}", songs.len(), blob_len);
            let mut tracks = blobs.into_iter();

            let mut items = Vec::with_capacity(blob_len);

            trace!("Iterating over playlist songs for {}", legible);

            for song in songs.into_iter() {
                trace!("Getting next track");
                let track = match tracks.next() {
                    Some(track) => track,
                    None => {
                        warn!(
                            "Playlist '{}' has an unequal number of items",
                            legible,
                        );

                        break;
                    },
                };
                trace!("Got next track");

                items.push(PlaylistItem {
                    info: song,
                    track,
                });
            }

            trace!("Iterated over playlist songs");

            libraries.insert(legible.to_lowercase(), Library {
                name: legible,
                items,
            });
        }

        // Calculate the list of names once to keep a cache and avoid
        // re-calculating it multiple times in the future.
        let mut names = libraries.values()
            .map(|library| library.name.clone())
            .collect::<Vec<_>>();
        names.sort();
        let list = names.join(", ");

        Ok(Self {
            list,
            libraries,
        })
    }
}
